#include <SDL2/SDL.h>

#include <windows.h>
#include <guiddef.h>

extern "C" {
	#include <libavcodec\avcodec.h>
	#include <libavutil\pixdesc.h>
	#include <libavutil\imgutils.h>
	#include <libswscale\swscale.h>
	#include <libswresample\swresample.h>
	#include <libavutil\samplefmt.h>
	#include <libavformat\avformat.h>
	
	#include <xaudio2redist.h>

}
//#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>
#include <tuple>



struct Audio;
struct meme
{
	Audio * a;
	int n;
};

struct Audio
{
	class VoiceCallback : public IXAudio2VoiceCallback {
		public:
	    HANDLE hBufferEndEvent;
	    VoiceCallback(): hBufferEndEvent( CreateEventA( NULL, FALSE, FALSE, NULL ) ){}
	    ~VoiceCallback(){ CloseHandle( hBufferEndEvent ); }
	
	    [[maybe_unused, gnu::cold]]void OnStreamEnd() { SetEvent( hBufferEndEvent ); }
	
	    [[maybe_unused, gnu::cold]]void OnVoiceProcessingPassEnd() { }
	    [[maybe_unused, gnu::cold]]void OnVoiceProcessingPassStart(UINT32 SamplesRequired) {    }
	    void OnBufferEnd(void * pBufferContext)    
		{ 
			meme * o = (meme*)pBufferContext; 
			std::get<1>(o->a->sounds[o->n]) = false; 
			//std::cout << o->n << '\n';
			delete o;
			SetEvent( hBufferEndEvent ); 
		}
	    [[maybe_unused, gnu::cold]]void OnBufferStart(void * pBufferContext) {   }
	    [[maybe_unused, gnu::cold]]void OnLoopEnd(void * pBufferContext) {    }
	    [[maybe_unused, gnu::cold]]void OnVoiceError(void * pBufferContext, HRESULT Error) { }
	};

	IXAudio2* root;
	IXAudio2MasteringVoice* master;
	XAUDIO2_VOICE_SENDS keys, bgm;
	WAVEFORMATEX wfx;
	std::vector<std::tuple<IXAudio2SourceVoice*, bool>> sounds;
	VoiceCallback voiceCallback{};

};

struct Media
{
	union
	{
		SDL_Surface * s;
		XAUDIO2_BUFFER x;
	};
	unsigned char * b;
};

std::vector<AVFrame*> * gen_decode (char * fname)
{	
	AVFormatContext * fc = nullptr;
	std::vector<AVPacket*> pv;
	std::vector<AVFrame*> *  fv = nullptr;
	AVCodec * cdc = nullptr;
	AVCodecContext * cc = nullptr;
	//bool err {1};
	AVMediaType type {AVMEDIA_TYPE_AUDIO};
	int sn;

	static auto decode = [&] (AVPacket * p/*, AVFrame ** f*/) -> bool { 
		bool error {1};
		AVFrame * t = av_frame_alloc();	
		int ret {};
		ret = avcodec_send_packet(cc, p);
		//std::cout << ret << '\n';
		if (ret < 0 || ret == AVERROR(EAGAIN))
		{
			error = 0;
			goto _exit1;
		}

		while (ret >= 0)
		{
			ret = avcodec_receive_frame(cc, t);
			if ((type == AVMEDIA_TYPE_VIDEO && t->width > 0)  || 
				(type == AVMEDIA_TYPE_AUDIO && t->sample_rate > 0))
			{
				AVFrame * f = av_frame_alloc();
				av_frame_ref(f, t);
				av_frame_unref(t);
				fv->push_back(f);
			}
			if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
			{
				//std::cout << "EAGAIN\n";
				break;
			}
			else if (ret < 0 )
			{
				error = 0;
				break;
			}

		}	
		_exit1:
		av_frame_free(&t);
		return error;
	};

	fc= avformat_alloc_context();
	if (avformat_open_input(&fc, fname, NULL, NULL) < 0)
	{
		//std::cout << "XD\n";
		///err = 0;
		goto _exit0;
	}
	if (avformat_find_stream_info(fc, NULL) < 0)
	{
		//err = 0;
		goto _exit0;
	}

	for (unsigned i{}; i < fc->nb_streams; i++)
		if (type == AVMEDIA_TYPE_AUDIO && fc->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
			continue;
		else if (fc->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
			type = fc->streams[i]->codecpar->codec_type;


	sn = av_find_best_stream(fc, type, -1, -1, &cdc, 0);
	if (sn < 0 && !cdc)
	{
		//err = 0;
		goto _exit0;
	}
	cc = avcodec_alloc_context3(cdc);
	avcodec_parameters_to_context(cc, fc->streams[sn]->codecpar);

	if (!cc)
	{
		//err = 0;
		goto _exit0;
	}
	if ( avcodec_open2(cc, cdc, NULL) < 0 )
	{
		//err = 0;
		goto _exit0;
	}	

	fv = new std::vector<AVFrame*>;
	//std::cout << cdc->long_name;

	while (1)
	{
		AVPacket * pk = av_packet_alloc();
		if(av_read_frame(fc, pk) != 0)
		{
			av_packet_free(&pk);
			break;
		}
		pv.emplace_back(pk);
	}
	for (unsigned i{}; i < pv.size(); i++)
	{
		decode(pv[i]);
		av_packet_free(&pv[i]);
	}

	while(decode(NULL)) ;
	avcodec_flush_buffers(cc);	
	//std::cout << type;
	_exit0:
	if(cc) 
	{
		avcodec_close(cc);
		avcodec_free_context(&cc);
	}
	if(fc)
		avformat_close_input(&fc);
	//std::cout << "XD\n";
	return fv;
}


bool process_frames(std::vector<AVFrame *> * v, Media & m)
{
	m.b = nullptr;
	//std::cout << i << ") ";
	if ((*v)[0]->width)
	{
		m.s = nullptr;
		for (unsigned i{}; i < v->size(); i++)
		{
			auto & x {(*v)[i]};
			if (x->width && !m.s)
			{
				/*std::cout << *x->linesize 
				<< ' ' << av_get_pix_fmt_name((AVPixelFormat  )x->format)  
				<< ' '<<x->width << ' ' << x->height 
				<< '\n';*/
				unsigned char *dst_data[4];
				int dst_linesize[4];

				SwsContext *sws_ctx = sws_getContext(x->width, x->height, (AVPixelFormat)x->format,
						x->width, x->height, AV_PIX_FMT_RGB24,
						SWS_BILINEAR, NULL, NULL, NULL);
				if (!sws_ctx)
					return 0;
		
				av_image_alloc(dst_data, dst_linesize, x->width, x->height, AV_PIX_FMT_RGB24, 1);
				sws_scale(sws_ctx, (const uint8_t * const*)x->data,
					x->linesize, 0, x->height, dst_data, dst_linesize);

				m.b = dst_data[0];
				m.s = SDL_CreateRGBSurfaceWithFormatFrom(dst_data[0], x->width, x->height, 32, dst_linesize[0], SDL_PIXELFORMAT_RGB24);

				sws_freeContext(sws_ctx);
			}
			if(m.s) break;
		}
	}
	else
	{
		//CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
		unsigned char * sound[v->size()+1];
		std::size_t ssz[v->size()+2];
		std::memset(ssz, 0, sizeof(std::size_t)*(v->size()+1));
		ssz[v->size()+1] = 0;
		unsigned n {};
		while (n < v->size())
			if ((*v)[n]->sample_rate)
				break;
			else n++;
		if ((*v)[n]->channel_layout == 0)
			(*v)[n]->channel_layout = av_get_default_channel_layout((*v)[n]->channels);
		SwrContext *swr = swr_alloc_set_opts(NULL,  // we're allocating a new context
				AV_CH_LAYOUT_STEREO,  // out_ch_layout
				AV_SAMPLE_FMT_S16,    // out_sample_fmt
				44100,                // out_sample_rate
				(*v)[n]->channel_layout,// in_ch_layout
				(AVSampleFormat )(*v)[n]->format,   // in_sample_fmt
				(*v)[n]->sample_rate,   // in_sample_rate
				0,                    // log_offset
				NULL);                // log_ctx
		swr_init(swr);
		if (!swr) return 0;
		for(unsigned i{}; i < v->size(); i++)
		{			
			auto & x {(*v)[i]};
			if (x->sample_rate)
			{
				/*std::cout << *x->linesize << ' ' << x->format
				<< ' ' << av_get_sample_fmt_name((AVSampleFormat )x->format)  
				<< ' '<<x->nb_samples << ' ' << x->sample_rate
				<< ' ' << x->channel_layout << ' ' << x->channels << '\n';*/
				unsigned char * out_data;
				int out_smpls;
			   	out_smpls = av_rescale_rnd(swr_get_delay(swr, x->sample_rate) +
						x->nb_samples, 44100, x->sample_rate, AV_ROUND_UP);
				av_samples_alloc(&out_data, 0, 2, out_smpls,
						AV_SAMPLE_FMT_S16, 0);
				ssz[i] = swr_convert(swr, &out_data, out_smpls,
					const_cast<const unsigned char**>(x->data), x->nb_samples);
				ssz[i]*=4;
				ssz[v->size()+1] += ssz[i];
				sound[i] = out_data;
			}
			else { ssz[i] = 0; sound[i] = 0; }
		}

		ssz[v->size()] = 0;
		sound[v->size()] = nullptr;
		if (auto x {swr_get_out_samples(swr, 0)}; x > 0)
		{
			unsigned char * out_data;
			av_samples_alloc(&out_data, 0, 2, x,
						AV_SAMPLE_FMT_S16, 0);
			ssz[v->size()] = swr_convert(swr, &out_data, x, 0, 0);
			ssz[v->size()]*=4;
			ssz[v->size()+1] += ssz[v->size()];
			sound[v->size()] = out_data;
		}
		unsigned char * outb {new unsigned char[ssz[v->size()+1]]};
		unsigned char * p = outb;
		for(unsigned i{}; i < v->size()+1; i++)
			if (ssz[i]) 
			{ 
				std::memcpy(p, sound[i], ssz[i]);
				p+=ssz[i];
				av_freep(&sound[i]);

			}
		//HRESULT hr;
		m.x = {0};
		m.x.AudioBytes = ssz[v->size()+1];
		m.x.pAudioData = outb;
		m.x.Flags = XAUDIO2_END_OF_STREAM;
		//m.x.pContext = &a;
		m.b = outb;
		swr_free(&swr);

		//delete [] outb;
	
	}
	return 1;
}


		

/*int jmain(int argc, char *argv[])
{
	Media m;
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
	HRESULT hr;
	if ( FAILED(hr = XAudio2Create( &a.root, 0, XAUDIO2_DEFAULT_PROCESSOR ) ) )
	    return hr;
	if ( FAILED(hr = a.root->CreateMasteringVoice( &a.master ) ) )
	    return hr;
	IXAudio2SubmixVoice * as
	a.root->CreateSubmixVoice(&pSFXSubmixVoice,2,44100,0,0,0,0);
	XAUDIO2_SEND_DESCRIPTOR SFXSend = {0, pSFXSubmixVoice};
	XAUDIO2_VOICE_SENDS SFXSendList = {1, &SFXSend};
	a.wfx.wFormatTag = WAVE_FORMAT_PCM;
	a.wfx.nChannels = 2;
	a.wfx.nSamplesPerSec = 44100;
	a.wfx.wBitsPerSample = 16;
	a.wfx.nAvgBytesPerSec = 44100*2*2;
	a.wfx.nBlockAlign = 4;//(wfx.nChannels*wfx.wBitsPerSample)/8;
	a.wfx.cbSize = 0;
	CoUninitialize();
	std::vector<AVFrame*> * vec = gen_decode("90s_003.ogg");
	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS);	

	SDL_Window * win = SDL_CreateWindow("k", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 256, 256, 0);
	process_frames(vec, m);
	for (int i{}; i < vec->size(); i++)
	{
		av_frame_free(&(*vec)[i]);
	}
	

	bool b {1};
	while (b)
	{
		SDL_Event e;
		while(SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
				{b = 0; break;}
			else if (e.type == SDL_KEYDOWN)
			{
				auto n {std::find_if(a.sounds.begin(), a.sounds.end(), 
					[] (std::tuple<IXAudio2SourceVoice*, bool> o) {return std::get<1>(o) == 0;} )};
				IXAudio2SourceVoice * t;	
				if( FAILED(hr = a.root->CreateSourceVoice( &t, (WAVEFORMATEX*)&a.wfx,
					0, XAUDIO2_MAX_FREQ_RATIO, &a.voiceCallback, NULL, NULL ) ) ) return hr;
				meme * mem {new meme{&a, (n == a.sounds.end()?a.sounds.size():(n-a.sounds.begin()))}};
				m.x.pContext = mem;
				if( FAILED(hr = t->SubmitSourceBuffer( &m.x ) ) )
				return hr;
				if ( FAILED(hr = t->Start( 0 ) ) )
					return hr;
				if (n == a.sounds.end())
				{
					a.sounds.push_back(std::make_tuple(t, true));
				}
				else 
				{
					std::get<0>(*n)->DestroyVoice();
					*n = std::make_tuple(t, true);
				}
			}
		}
	}
	WaitForSingleObjectEx( a.voiceCallback.hBufferEndEvent, INFINITE, 1 );

	delete vec;
	delete [] m.b;
	return 0;

}*/


