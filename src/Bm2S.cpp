#include <iostream>
#include "decode.hpp"

#include <SDL2\SDL_ttf.h>

#include <initguid.h>
extern "C" {
	#include <xapofx.h>
	#ifdef USE_HIGH_TIMER_FREQ
	NTSYSAPI NTSTATUS NTAPI
	NtSetTimerResolution(
	  IN ULONG                DesiredResolution,
	  IN BOOLEAN              SetResolution,
	  OUT PULONG              CurrentResolution );

	NTSYSAPI NTSTATUS NTAPI
	NtQueryTimerResolution(
  	  OUT PULONG              MinimumResolution,
 	  OUT PULONG              MaximumResolution,
 	  OUT PULONG              CurrentResolution );
	#endif

}

#include <fstream>
#include <sstream>
#include <string>
#include <thread>
#include <cmath>
#include <csignal>


template<class CharT>
constexpr short b36tb10(CharT c, std::size_t from=0, std::size_t to=1)
{
	short t {};
	if(c[from] >= '0' && c[from] <= '9')
		t = c[from] - '0';
	else if (c[from] >= 'a' && c[from] <= 'z')
		t = 10 + c[from] - 'a';
	else if (c[from] >= 'A' && c[from] <= 'Z')
		t = 10 + c[from] - 'A';
	return from < to ? t*36 + b36tb10(c, from+1) : t;
}


static char playerlevel, rank, *dir;
static std::string genre, title, artist, stagefile;

static double bpml[1296];
static Media sndl[1296]; 
static Media imgl[1296];
static Audio xaud;
static int stops[1296]/*, lnobj {-1}*/;

static unsigned short maxtrack {}, imgc {}, sndc{};
static HANDLE timer_driver;

static SDL_RWops * fntrwops;
//todo raii
static TTF_Font * fnt;
static SDL_Window * win;
static SDL_Renderer *rend;
static SDL_Texture * trtx, *lttx[3], * bgatex, * jutex[5], * numtex[10], * bpmtex;
static SDL_Surface *bgasur;


struct obj {
	unsigned short note;
	short time;
};

struct {
	std::vector <obj> bgmv;
	std::vector <obj> bgav[3];
	std::vector <obj> keys[16]; //p1 
	std::vector <obj> bpmv;
	double meas = 1;
} track[1000];


char* getfpath(char * fname/*, char (&ext)[16]*/)
{
	static char dirlen = std::strlen(dir);	
	char * rets = new char[dirlen + std::strlen(fname) + 3] {};
	std::strcpy(rets, dir);
	//./../jopa/wow.wav
	//mom/jopa/...
	static auto finlastsl = [=] (int j) {
		for(int i {dirlen}; i >= 0; i--)
			if (dir[i] == '\\' && i < j)
				return i;
		return -1;
	};
	int newindex {dirlen}, ldoti {-1};
	char * newfname = fname; 
	for(int i {}; i <= std::strlen(fname); i++) 
		if (fname[i] == '.')
		{
			if (fname[i+1] == '.' && (fname[i+2] == '\\' || fname[i+2] == '/'))
			{
				newfname+=3; i+=2;
				newindex = finlastsl(newindex);
			}
			else if (fname[i+1] == '\\' || fname[i+1] == '/')
				newfname+=2, i++;
			else ldoti = i;
		}
		else if (fname[i] == '/')
			fname[i] = '\\';
	rets[newindex++] = '\\';
	int newindex1 {newindex};

	for (int i {newindex}; i < (newindex + std::strlen(newfname)); i++)
	{
		rets[i] = newfname[i-newindex];
		if (newfname[i-newindex] == '\\')
			newindex1 = i;
	}
	
	rets[newindex+ldoti+1] = '*';
	rets[newindex+ldoti+2] = '\0';
	
	WIN32_FIND_DATAA fd;
	FindClose(FindFirstFileA(rets, &fd));

	int corr {newindex1-newindex?1:0};
	
	for (int i {newindex1+corr}; i <= (newindex1 + std::strlen(fd.cFileName)+corr); i++)
		(rets[i] = fd.cFileName[i-newindex1-corr]);

	rets[newindex1 + std::strlen(fd.cFileName)+corr] = 0;
	return rets;
}



bool parseline(char * line)
{

//genre title artist bpm playerlevel rank wav bmp stop lnobj stagefile

	std::istringstream is {line};
//	std::cout << line << '\n';

	if(is.get() != '#')
		return 0;
	if (std::isalpha(is.peek()))
	{
		std::string head {};
		char tst[12] {};

		is.get(tst, 12, ' ');
		head = tst;
		for(auto i = head.begin(); i != head.end(); i++)
			*i = std::tolower(*i);
	
		if (head == "genre")
			is >> genre;
		else if (head == "title")
			is >> title;
		else if (head == "artist")
			is >> artist;
		else if (head == "playlevel")
			is >> playerlevel;
		else if (head == "rank")
			is >> rank;
		else if (head == "stagefile")
			is >> stagefile;
		/*else if (head == "lnobj")
		{
			char temp[2];
			is >> temp;
			lnobj = b36tb10(temp);
		}*/ 
	
		else if (head.substr(0,3) == "bpm")
		{
			if(is.gcount() == 3)
				is >> bpml[0];
			else 
				is >> bpml[b36tb10(head,3,4)];
		}
		else if (head.substr(0,3) == "wav")
		{
			char sfilename[96] {}, *wavpath;
			while(is.peek() == ' ') (void)is.get();
			if(is.rdbuf()->sgetn(sfilename, 96) == 96)
				std::cout << "Does \"" << line << "\" look OK?\n";
			wavpath = getfpath(sfilename);	

			std::vector<AVFrame*> * outv;	
			if( (outv = gen_decode(wavpath)) == nullptr )
				{std::cout << sfilename << " does not work\n";}
			else 
			{
				process_frames(outv, sndl[b36tb10(head,3,4)]);
				sndc++;
				for (int i{}; i < outv->size(); i++)
					av_frame_free(&(*outv)[i]);
				delete outv;
				delete[] wavpath;
			}

		}
		else if (head.substr(0,3) == "bmp")
		{
			char sfilename[96] {}, *imgpath;
			while(is.peek() == ' ') (void)is.get();
			if(is.rdbuf()->sgetn(sfilename, 96) == 96)
				std::cout << "Does \"" << line << "\" look OK?\n";
			imgpath = getfpath(sfilename);
			//std::cout << imgpath << '\n';
			std::vector<AVFrame*> * outv;
			if( (outv = gen_decode(imgpath))== nullptr )
				{ std::cout << imgpath << " does not work\n"; }
			else 
			{
				process_frames(outv, imgl[b36tb10(head,3,4)]);
				SDL_SetColorKey(imgl[b36tb10(head,3,4)].s, SDL_TRUE, SDL_MapRGB(imgl[b36tb10(head,3,4)].s->format, 0,0,0));
				imgc++;
				for (int i{}; i < outv->size(); i++)
					av_frame_free(&(*outv)[i]);
				delete outv;
				delete[] imgpath;
			}
		}
		else if (head.substr(0,4) == "stop")
			is >> stops[b36tb10(head,4,5)];
		else return 1;
	}
	else
	{
		char head[5], body[512];
		is.get(head, 6, ':');
	
		short ntr {(head[0] - '0') * 100 + (head[1] - '0') * 10 + head[2] - '0'}, 
			nch {((head[3] - '0') * 10 + head[4] - '0')};

		if (nch == 5 || nch == 17 || (nch >= 21 && nch < 31) 
			|| nch == 37 || nch > 39 )  return 1;

		if (ntr > maxtrack)
			maxtrack = ntr;

		is.get();
		while(is.peek() == ' ')
			is.ignore();

		unsigned short rc {}, nrc {};
		rc = nrc = is.get(body, 512, '\n').gcount();

		short pr_vecs {};
		std::vector<obj> * vecp {nullptr};
		bool iskey {};
		switch(nch)
		{
			case 1:
				pr_vecs = track[ntr].bgmv.size();
				vecp = &track[ntr].bgmv;
			break;
			case 3: case 8: case 9:
				pr_vecs = track[ntr].bpmv.size();
				vecp = &track[ntr].bpmv;
			break;
			case 4:
				vecp = &track[ntr].bgav[0];
			break;
			case 6:
				vecp = &track[ntr].bgav[2];
			break;
			case 7:
				vecp = &track[ntr].bgav[1];
			break;
			default:
				if((nch >= 11 && nch <= 16) || nch == 18 || nch == 19)
				{
					int tnch {nch};
					if (tnch > 17)
						tnch--;
					pr_vecs = track[ntr].keys[tnch-11].size();
					vecp = &track[ntr].keys[tnch-11];
					iskey = true;
					break;
				}
				else if ((nch >= 31 && nch <= 36) || nch == 38 || nch == 39)
				{
					int tnch {nch};
					if (tnch > 37)
						tnch--;
					pr_vecs = track[ntr].keys[tnch-23].size();
					vecp = &track[ntr].keys[tnch-23];
					iskey = true;
					break;
				}
				else if (nch == 2)
				{
					std::string lole {body};
					track[ntr].meas = std::stof(lole);
					return 0;		
				}
		}

		for(int i {0}; i < rc - 1; i+=2)
		{
			if (body[i] == ' ')
			{	
				i++;
				nrc--;
				continue;
			}
			short curnote (0);
			if (nch != 3) curnote = b36tb10(body, i, i+1);  
			else {
 				if(body[i] >= '0' && body[i] <= '9')
					curnote = body[i] - '0';
				else if (body[i] >= 'a' && body[i] <= 'f')
					curnote = 10 + body[i] - 'a';
				else if (body[i] >= 'A' && body[i] <= 'F')
					curnote = 10 + body[i] - 'A';
				curnote*=16;
				if(body[i+1] >= '0' && body[i+1] <= '9')
					curnote += body[i+1] - '0';
				else if (body[i+1] >= 'a' && body[i+1] <= 'f')
					curnote += 10 + body[i+1] - 'a';
				else if (body[i+1] >= 'A' && body[i+1] <= 'F')
					curnote += 10 + body[i+1] - 'A';	
			}
			vecp->push_back({curnote, 192});		
		}


		unsigned short notecount {nrc/2};
		if (notecount == 0)
			return 1;

		if(! (iskey && pr_vecs > 0) )
		{
			for(int i{pr_vecs}; i < vecp->size(); i++)
			{
				if (nch == 1) (*vecp)[i].time = (double)(i - pr_vecs)/(double)notecount * 192.0;
				else if (nch == 8) {
						      (*vecp)[i].time = (double)i/(double)(notecount+pr_vecs) * 192.0;
							  (*vecp)[i].time |= (1<<9);
				}
				else if (nch == 9) {
						      (*vecp)[i].time = (double)(i - pr_vecs)/(double)notecount * 192.0;
							  (*vecp)[i].time |= (1<<10);
				}
				else 	 (*vecp)[i].time = ((double)i/(double)(notecount+pr_vecs)) * 192.0;
			}
		}
		else
		{
			int tnch {nch};
			if (tnch > 17 || tnch > 37) 
				tnch--;
			for(int i{pr_vecs}; i < vecp->size(); i++)
			{
				int ttime = (double)(i - pr_vecs)/(double)notecount * 192.0, nnote {-1};
				for(int j{}; j <= pr_vecs; j++)
					if ((*vecp)[j].time == ttime)
						{ nnote = j; break; }
				if (nnote < 0)
					(*vecp)[i].time = ttime;
				else 
				{
					(*vecp)[nnote].note = (*vecp)[i].note;
					(*vecp)[i].time = 255;
					std::cout << "check\n";	
				}		
			}
		}
	}

	return 0;
}


inline void render_screen_tx (SDL_Renderer * (&rend), SDL_Rect & scrr, SDL_Rect & trr, SDL_Rect (& keysrs)[8], 
	SDL_Rect & bgar, SDL_Texture * (&trtx), bool init)
{
	SDL_Rect tempr;
	trr.w = scrr.w*0.36;
	trr.h = scrr.h*0.9;
	trr.x = scrr.w*0.04;
	trr.y = scrr.h*0.04;
	if(init) trtx = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, scrr.w, scrr.h);
	SDL_SetRenderTarget(rend, trtx);
	SDL_SetRenderDrawColor(rend, 64,64,64,255);
	SDL_RenderClear(rend);

	SDL_SetRenderDrawColor(rend, 25,25,25, 255);
	tempr = {trr.x+1, trr.y+1,trr.w-2,trr.h-2};
	SDL_RenderFillRect(rend, &tempr);
	SDL_SetRenderDrawColor(rend, 255,255,255, 255);
	SDL_RenderDrawRect(rend, &trr);
	tempr = {trr.x-1, trr.y-1,trr.w+2,trr.h+2};
	SDL_RenderDrawRect(rend, &tempr);
	SDL_SetRenderDrawColor(rend, 225, 225, 225, 255);
	tempr = {std::floor(trr.w * (2.f/9.f))+1, trr.y, 0, trr.y + trr.h - 1 };
	int rest = std::ceil(((float)(trr.w - tempr.x-1))/7.f) * 7;
	tempr.x = tempr.w = (trr.x + trr.w - rest);
	int big {tempr.x - trr.x};
	SDL_RenderDrawLine(rend, tempr.x, tempr.y, tempr.w, tempr.h);
	for(int i {3}; i < 9; i++)
	{	
		tempr.w = (tempr.x += (rest/7));
		SDL_RenderDrawLine(rend, tempr.x, tempr.y, tempr.w, tempr.h);
	}
	//keys
	SDL_Color kc[2] = { {8,8,8,255}, {255,255,255,255} };
	keysrs[0] = { trr.x, scrr.h*0.96, big, scrr.h*0.03 };
	SDL_SetRenderDrawColor (rend, 255, 32, 32, 255);
	SDL_RenderFillRect(rend, &keysrs[0]);
	SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);	
	SDL_RenderDrawRect(rend, &keysrs[0]);
	for (int i {1}; i < 8; i++)
	{
		bool c {i%2};
		keysrs[i].x = keysrs[i-1].x + keysrs[i-1].w;
		keysrs[i].y = keysrs[0].y;
		keysrs[i].w = rest/7;
		keysrs[i].h = keysrs[i-1].h;
		SDL_SetRenderDrawColor(rend, kc[c].r, kc[c].g, kc[c].b, kc[c].a);	
		SDL_RenderFillRect(rend, &keysrs[i]);
		if (!c) {
			SDL_SetRenderDrawColor(rend, 192, 255, 192, 168);	
			SDL_RenderDrawRect(rend, &keysrs[i]);
		}
	}
	
	//rest
	bgar.w = bgar.h = std::exp2(std::trunc(std::log2(scrr.w*0.6 /*- scrr.w*0.025*/ - trr.w*0.05 - scrr.w*0.01)));
	bgar.y = (scrr.h - bgar.h - scrr.h*0.1 /*text height*/ )/2/* - scrr.h*0.05*/;												/*^ process bar*/
	bgar.x = trr.w + trr.x + (scrr.w*0.6 + scrr.w*0.01 + trr.w*0.05/* + scrr.w*0.025*/ - bgar.w)/2;
	SDL_SetRenderDrawColor(rend, 128, 128, 128, 255);
	SDL_RenderFillRect(rend, &bgar);
		
	//SDL_SetRenderTarget(rend, 0);
}

inline void render_light_txs (SDL_Renderer * (&rend), SDL_Rect & trr, SDL_Rect (&keysrs)[8], SDL_Rect (&ltr)[8], 
		SDL_Texture * (&lttx)[3], bool init)
{
	//light
	SDL_Rect tempr;
	SDL_SetRenderDrawBlendMode(rend, SDL_BLENDMODE_BLEND);
	ltr[0].x = trr.x+1;
	ltr[0].w = keysrs[0].w-1;
	ltr[0].h = std::ceil((double)trr.h/1.25);
	ltr[0].y = trr.y + trr.h - ltr[0].h - 1;
	if (init) lttx[0] = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, ltr[0].w, ltr[0].h);
	SDL_SetRenderTarget(rend, lttx[0]);
	SDL_SetRenderDrawColor(rend, 0,0,0,0);
	SDL_RenderClear(rend);
	unsigned char alph {255};
	int l {255};
	while (l > 0 && ltr[0].h / l == 0)
			l>>=1;

	tempr = ltr[0];
	tempr.x = tempr.y = 0;
	tempr.h /= l;
	tempr.y += ltr[0].h;
	for (int i {}; i <= l; i++)
	{
		SDL_SetRenderDrawColor(rend, 32, 200, 128, alph);
		SDL_RenderFillRect(rend, &tempr);
		tempr.y -= tempr.h;
		alph -= 255/l;
	}
	for (int i{1}; i < 8; i++)
	{
		ltr[i] = ltr[i-1];
		ltr[i].x += ltr[i-1].w+1;
		ltr[i].w = keysrs[1].w-1;
	}

	SDL_Color lc[2] {{32, 32, 255, 255}, {24, 24, 168, 255}};
	for (int k{1}; k <= 2; k++)
	{
		tempr = ltr[k];
		tempr.y = tempr.x = 0;
		tempr.h /= l;
		tempr.y += ltr[k].h;
		if(init) 
			lttx[k] = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, ltr[k].w, ltr[k].h);
		SDL_SetRenderTarget(rend, lttx[k]);
		SDL_SetRenderDrawColor(rend, 0,0,0,0);
		SDL_RenderClear(rend);
		for (int i {}; i <= l; i++)
		{
			SDL_SetRenderDrawColor(rend, lc[k-1].r, lc[k-1].g, lc[k-1].b, lc[k-1].a);
			SDL_RenderFillRect(rend, &tempr);
			tempr.y -= tempr.h;
			lc[k-1].a -= 255/l;
		}
	}

	SDL_SetRenderDrawBlendMode(rend, SDL_BLENDMODE_NONE);
}

inline char prepare_num (TTF_Font * fnt, SDL_Rect (&r)[6], int (&t)[6], int n)
{
	static auto itoa = [=] (int num, int & b, int & d) -> char {
		char ret {};
		if((ret = (num / b) - d))
			d+=(num/b) - d;
		d *= 10;
		b /= 10;	
		return ret;	
	};
	r[5].x = r[5].w = 0; 
	int d {}, b{100000};
	int x { };
	bool bdr {};
	char nigger {5};
	for (int i {0}; i < 6; i++)
	{
		t[i] = itoa(n, b, d);
		if (t[i] > 0)
		{
			if(!bdr) nigger = i;
			bdr = true;
		}
		if (bdr)
		{
			int maxx {}, minx{}, miny{}, maxy{}, advance{};
			TTF_GlyphMetrics(fnt, t[i]+'0', &minx, &maxx, &miny, &maxy, &advance);
			r[i].x = x+minx;
			if (i == nigger ) { r[i].x += advance; x += advance; }
			r[i].y = TTF_FontAscent(fnt)-maxy;
			r[i].h = maxy-miny;
			r[i].w = maxx-minx;
			x += advance;
		}
	}
	return nigger;
}

__attribute__((hot))inline bool play_sound(XAUDIO2_VOICE_SENDS chan, int ns)
{
	auto n {std::find_if(xaud.sounds.begin(), xaud.sounds.end(), 
			[] (std::tuple<IXAudio2SourceVoice*, bool> o) {return std::get<1>(o) == 0;} )};
	IXAudio2SourceVoice * t;	
	if( FAILED(xaud.root->CreateSourceVoice( &t, (WAVEFORMATEX*)&xaud.wfx,
		0, XAUDIO2_MAX_FREQ_RATIO, &xaud.voiceCallback, &chan, NULL ) ) ) return 0;
	meme * mem {new meme{&xaud, (n == xaud.sounds.end()?xaud.sounds.size():(n-xaud.sounds.begin()))}};

	sndl[ns].x.pContext = mem;
	if( FAILED(t->SubmitSourceBuffer( &sndl[ns].x ) ) )
		return 0;
	if ( FAILED(t->Start( 0 ) ) )
		return 0;
	if (n == xaud.sounds.end())
	{
		xaud.sounds.push_back(std::make_tuple(t, true));
	}
	else 
	{
		std::get<0>(*n)->DestroyVoice();
		*n = std::make_tuple(t, true);
	}
	return 1;
}


void restorefreq(void)
{
	#ifdef USE_HIGH_TIMER_FREQ
	unsigned long asd {};
	NtSetTimerResolution(0, 0, &asd);
	#endif
	xaud.root->Release();
	for(int i{}; i < sndc; i++)
	{
		delete [] sndl[i].b;
	}
	SDL_FreeSurface(bgasur);
	for(int i{}; i < imgc; i++)
	{
		SDL_FreeSurface(imgl[i].s);
		av_freep(&imgl[i].b);
	}


	SDL_RWclose(fntrwops);
	SDL_DestroyTexture(bpmtex);
	SDL_DestroyTexture(bgatex);
	SDL_DestroyTexture(lttx[0]);
	SDL_DestroyTexture(lttx[1]);
	SDL_DestroyTexture(lttx[2]);
	SDL_DestroyTexture(trtx);
	for(int i{}; i < 5; i++)
		SDL_DestroyTexture(jutex[i]);
	for(int i{}; i <= 9; i++)
		SDL_DestroyTexture(numtex[i]);

    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(win);
	TTF_CloseFont(fnt);	

	TTF_Quit();
	SDL_Quit();
	std::cout << "App successfully terminated.\n";
}

int l2(int t1, int t2, int tr1, int tr2)
{
	if (tr1 == tr2) if(t1 > t2) return  (t1 - t2); else return  (t2 - t1);
	else if (tr1 > tr2) 
	{
		int ret {0};
		for (int i {tr2}; i < tr1; i++)
			ret += 192/*track[i].meas*/;
		return ret - t2 + t1;
	}
	else if (tr1 < tr2) 
	{
		int ret {0};
		for (int i {tr1}; i < tr2; i++)
			ret += 192/*track[i].meas*/;
		return ret + t2 - t1;
	}
}


int main(int argc, char ** argv)	
{
	double fspeed { 5.55 };
	bool autoplay {0};
	float bgmvol {0.9}, keyvol {1};
	int pathn {1};

	std::signal(SIGINT, SIG_IGN);


	#ifdef DEBUG_NOARG
	//char testpath[] = "C:\\Users\\furuderika\\Desktop\\dogshit\\[paraoka] L9\\9_7l.bms";
//	char testpath[] = "D:\\games\\LR2_100201\\LR2beta3\\THE BMS OF FIGHTERS 2008\\[sasakuration]AVALON(MQ)\\01_avalon[light7].bme";
//	char testpath[] = "D:\\games\\LR2_100201\\LR2beta3\\zsx\\x\\X(7-2DX).bme";
//	char testpath[] = "C:\\Users\\furuderika\\Desktop\\dogshit\\e rev\\erev.bme";
//	char testpath[] = "D:\\games\\LR2_100201\\LR2beta3\\THE\ BMS\ OF\ FIGHTERS\ 2006\\curryyojo_Splashcode\\02_Splashcode_N7.bme";
//	char testpath[] = "C:\\Users\\furuderika\\Desktop\\dogshit\\[t+pazolite]bof_lsb_HQ\\_little_sister_bitch_7_bga.bme";
	char testpath[] = "D:\\games\\LR2_100201\\LR2beta3\\THE\ BMS\ OF\ FIGHTERS\ 2009\\xi_mirage_garden\\m_garden_L.bme";
//	char testpath[] = "D:\\games\\LR2_100201\\LR2beta3\\beatmania IIDX14 GOLD\\Fascination MAXX\\Fascination MAXX(2 A).bms";
	argv[1] = testpath;
	argc = 2;
	#else
	if(argc < 2)
		return 1;
	for (; pathn < argc; pathn++)
	{
		if (argv[pathn][0] == '-')
			switch(argv[pathn][1])
			{
				case 'A': 
					if (argv[pathn][2] == '\0')
						autoplay = 1;
					else std::cout << "Illegal argument '" << argv[pathn] <<"', ignoring\n";
				break;
				case 'v':
					switch(argv[pathn][2])
					{
						case 'K':
							if (argv[pathn][3] == '\0')
								if (int t {atoi(argv[++pathn])}; t!=0)
										keyvol = t/100.f;
								else std::cout << "Invalid value to argument 'vK': " << argv[pathn] << std::endl;
							else std::cout << "Illegal argument '" << argv[pathn] <<"', ignoring\n";
						break;
						case 'B':
							if (argv[pathn][3] == '\0')
								if (int t {atoi(argv[++pathn])}; t!=0)
										bgmvol = t/100.f;
								else std::cout << "Invalid value to argument 'vB': " << argv[pathn] << std::endl;
							else std::cout << "Illegal argument '" << argv[pathn] <<"', ignoring\n";
						break;
					}

				break;
				case 's':
					if (argv[pathn][2] == '\0') {
						if(float t {std::atof(argv[++pathn])}; t != 0.0)
							fspeed = t;
						else std::cout << "Invalid value to argument 's': " << argv[pathn] << std::endl;
					}
					else std::cout << "Illegal argument '" << argv[pathn] <<"', ignoring\n";
				break;
			}
	}
	pathn-=1;
	#endif

	std::ifstream ifs{argv[pathn]};
	if (ifs.fail())
		return 1;
	dir = new char[std::strlen(argv[pathn])];
	bool rtcp {};
	for(int i {std::strlen(argv[pathn])}; i >= 0; i--)
		if(rtcp)
			dir[i] = argv[pathn][i];
		else if(!rtcp && argv[pathn][i] == '\\')
		{
			dir[i] = 0;
			rtcp = true;
		}




	CoInitializeEx(NULL, COINIT_MULTITHREADED);
	HRESULT hr;
	if ( FAILED(hr = XAudio2Create( &xaud.root, 0, XAUDIO2_DEFAULT_PROCESSOR ) ) )
	    return hr;
	if ( FAILED(hr = xaud.root->CreateMasteringVoice( &xaud.master ) ) )
	    return hr;
	IXAudio2SubmixVoice * xasv0, * xasv1;
	xaud.root->CreateSubmixVoice(&xasv0,2,44100,0,0,0,0);
	xaud.root->CreateSubmixVoice(&xasv1,2,44100,0,0,0,0);
	xasv1->SetVolume(bgmvol); 	xasv0->SetVolume(keyvol);
	XAUDIO2_SEND_DESCRIPTOR xasd0 {0, xasv0}, xasd1 {0, xasv1};
	xaud.keys.SendCount = 1;
	xaud.keys.pSends = &xasd0;
	xaud.bgm.SendCount = 1;
	xaud.bgm.pSends = &xasd1;
	xaud.wfx.wFormatTag = WAVE_FORMAT_PCM;
	xaud.wfx.nChannels = 2;
	xaud.wfx.nSamplesPerSec = 44100;
	xaud.wfx.wBitsPerSample = 16;
	xaud.wfx.nAvgBytesPerSec = 44100*2*2;
	xaud.wfx.nBlockAlign = 4;//(wfx.nChannels*wfx.wBitsPerSample)/8;
	xaud.wfx.cbSize = 0;

	IUnknown * pXAPO;
	CreateFX(CLSID_FXMasteringLimiter, &pXAPO);
	XAUDIO2_EFFECT_DESCRIPTOR xapod;
	xapod.InitialState = true;
	xapod.OutputChannels = 2;
	xapod.pEffect = pXAPO;
	XAUDIO2_EFFECT_CHAIN xapoc;
	xapoc.EffectCount = 1;
	xapoc.pEffectDescriptors = &xapod;
	xaud.master->SetEffectChain(&xapoc);
	pXAPO->Release();
	FXMASTERINGLIMITER_PARAMETERS limpara {10, 450};
	xaud.master->SetEffectParameters(0, &limpara, sizeof(FXMASTERINGLIMITER_PARAMETERS));
	CoUninitialize();


	int totalnotes {};
	std::thread parser_thread { [&] {
		char lin[1024];
		while(!ifs.eof())
		{
			ifs.getline(lin, 1024, '\n');
			if(parseline(lin))
				std::cout << "Could not parse: " << lin << std::endl;
		}

		for(int i {}; i <= maxtrack; i++)
		{
			auto comp = [] (obj a, obj b) {return a.time < b.time;};
			auto deleter = [] (decltype (track[i].bgmv) & v) 
			{
				for (auto i{v.begin()}; i != v.end(); )
					if((i->note == 0 || i->note >= 1296))
						i = v.erase(i);
					else i++;
			};
			deleter(track[i].bgmv);
			deleter(track[i].bpmv);
			deleter(track[i].bgav[0]);
			deleter(track[i].bgav[1]);
			deleter(track[i].bgav[2]);

			//reimplement
			for (auto & x: track[i].bpmv )
			{
				if(x.time & (1<<9)) 
				{
					x.note = bpml[x.note];
					x.time ^= (1<<9);
				}
				else if(x.time & (1<<10)) 
					x.note = stops[x.note];
			}
			
			for(int j{}; j < 16; j++)
			{
				deleter(track[i].keys[j]);
				if(j < 8) totalnotes += track[i].keys[j].size();
				std::sort(track[i].keys[j].begin(), track[i].keys[j].end(), comp);
			}
			std::sort(track[i].bgav[0].begin(), track[i].bgav[0].end(), comp);
			std::sort(track[i].bgav[1].begin(), track[i].bgav[1].end(), comp);
			std::sort(track[i].bgav[2].begin(), track[i].bgav[2].end(), comp);
			std::sort(track[i].bgmv.begin(), track[i].bgmv.end(), comp);
			std::sort(track[i].bpmv.begin(), track[i].bpmv.end(), comp);
		}
	}};


	TTF_Init();

	SDL_Surface *curbga4 {nullptr}, *curbga7{nullptr};
	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS);
	SDL_Rect scrr;
	SDL_GetDisplayBounds(0, &scrr);

	#ifdef FULLSCREEN
	scrr.w = (double)scrr.h/3.0*4.0;
	#else
	scrr.w = 800; scrr.h = 600;
	#endif

	win = SDL_CreateWindow 
	("Bm2S", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, scrr.w, scrr.h, 
	#ifdef FULLSCREEN	
	SDL_WINDOW_FULLSCREEN|
	#endif
	SDL_WINDOW_RESIZABLE);
	rend = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_TARGETTEXTURE);
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0");
	SDL_SetHint(SDL_HINT_RENDER_DRIVER, "direct3d");

	SDL_Rect trr, keysrs[8], bgar, ltr[8],
		judger[5], procr, bpmr;

	render_screen_tx(rend, scrr, trr, keysrs, bgar, trtx, 1);
	render_light_txs(rend, trr, keysrs, ltr, lttx, 1);
	SDL_SetTextureBlendMode(trtx, SDL_BLENDMODE_BLEND);
	SDL_SetTextureBlendMode(lttx[0], SDL_BLENDMODE_BLEND);
	SDL_SetTextureBlendMode(lttx[1], SDL_BLENDMODE_BLEND);
	SDL_SetTextureBlendMode(lttx[2], SDL_BLENDMODE_BLEND);

	bgasur = SDL_CreateRGBSurfaceWithFormat(0, 256, 256, 32, SDL_PIXELFORMAT_RGB24);
	bgatex = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING, 256, 256);
	void * bgapix; int bgapit;

	HRSRC fntsrc = FindResourceA(NULL, MAKEINTRESOURCEA(2), RT_FONT);
	if (fntsrc == NULL)
		return 1;
	HGLOBAL fntres = LoadResource(NULL, fntsrc);
	const void * fntp = LockResource(fntres);
	DWORD fntsz = SizeofResource(NULL, fntsrc);
	fntrwops = SDL_RWFromConstMem(fntp, fntsz);
	fnt = TTF_OpenFontRW(fntrwops, 0, scrr.h*0.1);
	int fw, fh;
	char judge_text[6][7] = {"cool", "great", "good", "bad", "poor", "bpm"};
	SDL_Color judge_col[5] = {
		{64, 255, 64, 255},
		{255, 255, 255, 255},
		{255, 255, 255, 255},
		{255, 64, 64, 255},
		{192, 192, 192, 255}
	};
	SDL_Surface * tsur;
	for(int i{}; i < 5; i++)
	{
		tsur = TTF_RenderText_Blended(fnt, judge_text[i], judge_col[i]);
		TTF_SizeText(fnt, judge_text[i], &fw, &fh);
		judger[i] = {0, 0, fw, fh};
		jutex[i] = SDL_CreateTextureFromSurface(rend, tsur);
		SDL_FreeSurface(tsur);
	}
	tsur = TTF_RenderText_Blended(fnt, judge_text[5], judge_col[1]);
	TTF_SizeText(fnt, judge_text[5], &fw, &fh);
	bpmr = {0, 0, fw, fh};
	bpmtex = SDL_CreateTextureFromSurface(rend, tsur);
	SDL_FreeSurface(tsur);
	for(int i{}; i <= 9; i++)
	{
		tsur = TTF_RenderGlyph_Blended(fnt, '0'+i, {255, 255, 255, 255});
		numtex[i] = SDL_CreateTextureFromSurface(rend, tsur);
		SDL_FreeSurface(tsur);
	}

	parser_thread.join();
	ifs.close();
	delete[] dir;

	double ppt { trr.h/(double)maxtrack };
	double curproc { };
	procr = { trr.x + trr.w*1.05, trr.y, scrr.w*0.01, trr.h };


	struct {
		struct {
			short tr;
			char nn;
			char prn;
		} nts[3];
		char prk;
	} keys [8];
	std::memset((void*)&keys, 0, sizeof(keys));
	
	bool sore {1};

	for(int k{}; k < 16; k++) //reimplement
	{
		unsigned char nnt {1}, tk {k};
		if (k > 7)
		{
			nnt = 2;
			tk-=8;
		}
		while(keys[tk].nts[nnt].tr <= maxtrack)
		{	
			if(keys[tk].nts[nnt].nn < track[keys[tk].nts[nnt].tr].keys[k].size())
			{
				keys[tk].nts[nnt].nn = keys[tk].nts[nnt].nn;
				break;
			}
			else
			{
				keys[tk].nts[nnt].tr++;
				keys[tk].nts[nnt].nn = 0;
			}
			if (keys[tk].nts[nnt].tr > maxtrack)
				keys[tk].nts[nnt].prn = 2;
		}
		
		if(nnt == 1) {
			keys[k].nts[0] = keys[k].nts[1];
			keys[k].nts[0].prn = 1;
		}
	}
	if (keys[6].nts[1].prn == 2 && keys[7].nts[1].prn == 2) { sore = 0; std::cout << "Non-extended BMS detected.\n"; }
	int jut{}; char jun {-1};
	std::vector<obj> defpoor{{0,0}};
	struct Miss {
		std::vector<obj> * bga;
		unsigned char i, c, n;
	} miss {(imgl[0].s?&defpoor:nullptr), (unsigned char)-1, 0, 0};
		
	unsigned combo { }, maxcombo { };
	unsigned hitcount[5] {0,0,0,0,0};

	unsigned int ctick {0}, truetime { }, neartime { };
	short ntrack {0};
	double curbpm { };
	int stoptime { };
	const char nkeys ((sore?8:6));

	#ifdef USE_HIGH_TIMER_FREQ
	unsigned long res {}, a,b;
	NtQueryTimerResolution (&a, &res, &b);
	std::cout << "Set HW timer resolution to " << (res/10000.0) << "ms.\n";

	NtSetTimerResolution(res, 1, &a);
	
	#endif
	SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS); //this is a nice schedule

	LARGE_INTEGER ft;
	if (track[ntrack].bpmv.size() && track[ntrack].bpmv[0].note > 0)
		curbpm = track[ntrack].bpmv[0].note;
	else
		curbpm = bpml[0];
	ft.QuadPart = -(120 * 10e4)/(curbpm);
	timer_driver = CreateWaitableTimer(NULL, TRUE, NULL);
	std::cout << "Timer is running at period of " << (ft.QuadPart*-10.0) << " microseconds. (BPM is " << curbpm <<")\n";

	SDL_Rect bpmnums[6];
	int bpmns[6] {0,0,0,0,0,0};
	int bpmnst = prepare_num(fnt, bpmnums, bpmns, curbpm);
	bpmr.x = bgar.x + (bgar.w - (bpmr.w + bpmnums[5].x + bpmnums[5].w))/2;
	bpmr.y = bgar.y + bgar.h*1.05;
	for (int i {bpmnst}; i < 6; i++)
	{
		bpmnums[i].x += bpmr.x + bpmr.h;
		bpmnums[i].y = bpmr.y - bpmnums[i].y + bpmr.h/2;
	}


	std::atexit(restorefreq);

	bool quitevent {};
	/*auto l1 = [=] (short t1, short t2, short tr1, short tr2) -> short
	{
		if (tr1 == tr2) if(t1 > t2) return  (t1 - t2); else return  (t2 - t1);
		else if (tr1 - tr2 == 1) return  (192.0*track[tr2].meas - t2 + t1);
		else if (tr1 - tr2 == -1)return (192.0*track[tr1].meas - t1 + t2);
		else return -10000;
	};*/


	
	SDL_PumpEvents();
	SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
	
	auto check_end = [=] () -> bool{
		//std::cout << xaud.sounds.size() << '\n';

		for (auto x: xaud.sounds)
			if(std::get<1>(x) == 1)
				return 1;
		return 0;
	};

	//std::ofstream os {"record.txt"};
	//return 0;
	while(!quitevent && (ntrack <= maxtrack || check_end()))
	{
		SetWaitableTimer(timer_driver, &ft, 0, 0, NULL, 1);
		truetime = ctick/track[ntrack].meas;
		neartime = (1+ctick)/track[ntrack].meas;
		double rel {curbpm/120.0};
		char tjun {};
		if (stoptime)
			goto skiptoevent;
		if(track[ntrack].bgmv.size())
		{
			for(auto x :track[ntrack].bgmv)
			{
				if (x.time > truetime && !(x.time < neartime)) break;
				if (x.time < truetime && !(x.time > neartime)) continue;
				if(!play_sound(xaud.bgm, x.note)) std::cout << "Failed to play sound " << x.note << ".\n";
			}
		}
		if(track[ntrack].bgav[0].size())
		{
			for(auto x :track[ntrack].bgav[0])
			{
				if (x.time > truetime && !(x.time < neartime)) break;
				if (x.time < truetime) continue;
				curbga4 = imgl[x.note].s;
			}	
		}
		if(track[ntrack].bgav[1].size())
		{
			for(auto x :track[ntrack].bgav[1])
			{
				if (x.time > truetime && !(x.time < neartime)) break;
				if (x.time < truetime) continue;

				curbga7 = imgl[x.note].s;
			}	
		}
		if(track[ntrack].bgav[2].size())
			miss.bga = &track[ntrack].bgav[2];

		for(int i{}; autoplay && i < nkeys; i++)
			if(track[ntrack].keys[i].size())
			{
				for(auto x :track[ntrack].keys[i])
				{
					if (x.time > truetime && !(x.time < neartime)) break;
					if (x.time < truetime) continue;

					if(!play_sound(xaud.keys, x.note)) 
						std::cout << "Failed to play sound " << x.note << ".\n";
				}
			}

		for(int k{}; !autoplay && k < nkeys*2; k++)
		{
			unsigned char nnt {1}, tk {k};
			short t0{}, t1{};
			if (k > 7)
			{
				nnt = 2;
				tk-=8;
				t0 = keys[tk].nts[nnt].nn; t1 = keys[tk].nts[nnt].tr;
			}
			if (keys[tk].nts[nnt].prn == 2)
				continue;


			if ((keys[tk].nts[nnt].tr == ntrack && 
				track[keys[tk].nts[nnt].tr].keys[k][keys[tk].nts[nnt].nn].time <= truetime) || 
				keys[tk].nts[nnt].tr < ntrack
			)
			{
				if(nnt == 1)
					keys[k].nts[0] = keys[k].nts[1];
				while(keys[tk].nts[nnt].tr <= maxtrack)
				{	
					if (keys[tk].nts[nnt].nn+1 < track[keys[tk].nts[nnt].tr].keys[k].size())
					{		
							keys[tk].nts[nnt].nn++;
							keys[tk].nts[nnt].prn = 0;
							break;
					}
					else
					{
						keys[tk].nts[nnt].tr++;
						keys[tk].nts[nnt].nn = -1;
					}
				}
				if (nnt == 1 && keys[tk].nts[1].tr > maxtrack)
				{
					keys[tk].nts[1] = keys[tk].nts[0];
					keys[tk].nts[1].prn = 2;
				}
				else if (nnt == 2 && keys[tk].nts[2].tr > maxtrack)
				{
					keys[tk].nts[2].nn = t0;
					keys[tk].nts[2].tr = t1;
					keys[tk].nts[2].prn = 2;
				}
			}
		}
		

		/******GRAPHICS****/ 
		SDL_SetRenderTarget(rend, 0);
		//SDL_SetRenderDrawColor(rend, 0, 0, 0, 0);
		SDL_RenderCopy(rend, trtx, 0, 0);
		SDL_RenderCopy(rend, bpmtex, 0, &bpmr);
		for (int i {bpmnst}; i < 6; i++)
			SDL_RenderCopy(rend, numtex[bpmns[i]], 0, &bpmnums[i]);
			
		SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
		SDL_RenderDrawRect(rend, &procr);
		procr.h = curproc;
		SDL_RenderFillRect(rend, &procr);
		procr.h = trr.h;

		skiptoevent:
		SDL_Event event;
		while(SDL_PollEvent( &event )) 
		{
			if(event.type == SDL_QUIT) {
				quitevent = true;
			}
			#define longline (event.type == SDL_KEYDOWN && event.key.keysym.scancode == SDL_SCANCODE_SPACE)
			if(longline || (event.type == SDL_WINDOWEVENT && 
				(event.window.event == SDL_WINDOWEVENT_HIDDEN || event.window.event == SDL_WINDOWEVENT_MINIMIZED)))
			{
				xaud.root->StopEngine(); 
				SDL_Event resizeonpause; 
				while(SDL_WaitEvent(&event))
				{
					/*const unsigned char * s = SDL_GetKeyboardState(0);
					if (s[SDL_SCANCODE_F] && s[SDL_SCANCODE_j]
					{*/
					if(longline || (event.type == SDL_WINDOWEVENT && 
					(event.window.event == SDL_WINDOWEVENT_SHOWN || event.window.event == SDL_WINDOWEVENT_RESTORED)))
					{
						xaud.root->StartEngine(); 
						SDL_PushEvent(&resizeonpause);
						break;
					}
					else if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED)
						resizeonpause = event;
				}
			}
			#undef longline
			
			if (event.type == SDL_WINDOWEVENT && event.window.windowID == SDL_GetWindowID(win))
			switch (event.window.event)
			{
				case SDL_WINDOWEVENT_EXPOSED:
					if (SDL_GetWindowFlags(win) & SDL_WINDOW_FULLSCREEN)
						goto skipsetting;
					else break;
				case SDL_WINDOWEVENT_RESIZED:
	                scrr.h = event.window.data2;
					scrr.w = event.window.data1;

					if (scrr.w/(float)scrr.h != 4/3.f)
					{
						float u = (scrr.w/4.f + scrr.h/3.f)/2.f;
						scrr.w = u*4; scrr.h = u*3;
						SDL_SetWindowSize(win, scrr.w, scrr.h);
					}
					skipsetting:
					SDL_DestroyTexture(bpmtex);
					SDL_DestroyTexture(lttx[0]);
					SDL_DestroyTexture(lttx[1]);
					SDL_DestroyTexture(lttx[2]);
					SDL_DestroyTexture(trtx);
					for(int i{}; i < 5; i++)
						SDL_DestroyTexture(jutex[i]);
					for(int i{}; i <= 9; i++)
						SDL_DestroyTexture(numtex[i]);
					render_screen_tx(rend, scrr, trr, keysrs, bgar, trtx, 1);
					render_light_txs(rend, trr, keysrs, ltr, lttx, 1);
					SDL_SetRenderTarget(rend, 0);
					TTF_CloseFont(fnt);
					SDL_RWseek(fntrwops, 0, RW_SEEK_SET);
					fnt = TTF_OpenFontRW(fntrwops, 0, scrr.h*0.1);
					for(int i{}; i < 5; i++)
					{
						tsur = TTF_RenderText_Blended(fnt, judge_text[i], judge_col[i]);
						TTF_SizeText(fnt, judge_text[i], &fw, &fh);
						judger[i] = {0, 0, fw, fh};
						jutex[i] = SDL_CreateTextureFromSurface(rend, tsur);
						SDL_FreeSurface(tsur);
					}
					tsur = TTF_RenderText_Blended(fnt, judge_text[5], judge_col[1]);
					TTF_SizeText(fnt, judge_text[5], &fw, &fh);
					bpmr = {0, 0, fw, fh};
					bpmtex = SDL_CreateTextureFromSurface(rend, tsur);
					SDL_FreeSurface(tsur);
					curproc /= ppt;
					ppt = trr.h/(double)maxtrack;
					curproc *= ppt;
					procr = { trr.x + trr.w*1.05, trr.y, scrr.w*0.01, trr.h };
					for(int i{}; i <= 9; i++)
					{
						tsur = TTF_RenderGlyph_Blended(fnt, '0'+i, {255, 255, 255, 255});
						numtex[i] = SDL_CreateTextureFromSurface(rend, tsur);
						SDL_FreeSurface(tsur);
					}
					bpmnst = prepare_num(fnt, bpmnums, bpmns, curbpm);
					bpmr.x = bgar.x + (bgar.w - (bpmr.w + bpmnums[5].x + bpmnums[5].w))/2;
					bpmr.y = bgar.y + bgar.h*1.05;
					for (int i {bpmnst}; i < 6; i++)
					{
						bpmnums[i].x += bpmr.x + bpmr.h;
						bpmnums[i].y = bpmr.y - bpmnums[i].y + bpmr.h/2;
					}
					SDL_SetTextureBlendMode(trtx, SDL_BLENDMODE_BLEND);
					SDL_SetTextureBlendMode(lttx[0], SDL_BLENDMODE_BLEND);
					SDL_SetTextureBlendMode(lttx[1], SDL_BLENDMODE_BLEND);
					SDL_SetTextureBlendMode(lttx[2], SDL_BLENDMODE_BLEND);
				break;
			}
			char keyn {-1};
			if(event.type == SDL_KEYDOWN || event.type == SDL_KEYUP)
			{
				switch(event.key.keysym.scancode)
				{
					case SDL_SCANCODE_S:
						keyn = 0;
					break;
					case SDL_SCANCODE_D:
						keyn = 1;
					break;
					case SDL_SCANCODE_F:
						keyn = 2;
					break;
					case SDL_SCANCODE_J:
						keyn = 3;
					break;
					case SDL_SCANCODE_K:
						keyn = 4;
					break;
					case SDL_SCANCODE_A:
						keyn = 5;
					break;
					case SDL_SCANCODE_L:
						keyn = 6;
					break;
					case SDL_SCANCODE_SEMICOLON:
						keyn = 7;
					break;
					case SDL_SCANCODE_ESCAPE:
						SDL_Event e1;
						e1.type = SDL_QUIT;
						SDL_PushEvent(&e1);
					break;
				}
				if (!sore)
				{
					switch (keyn)
					{
						case 7: keyn=-1;
						break;
						case 0: keyn=5;
						break;
						case 6: keyn=4;
						break;
						default: keyn=keyn-1;
						break;
					};
				}
				if(!autoplay && keyn > -1)
				{
					if(event.type == SDL_KEYDOWN) keys[keyn].prk = 127;
					else keys[keyn].prk = 8*rel;	
					if(!event.key.repeat && event.type == SDL_KEYDOWN)
					{ 
						int df[3];
						df[0] =  
							l2(track[keys[keyn].nts[0].tr].keys[keyn][keys[keyn].nts[0].nn].time,
								truetime,
								keys[keyn].nts[0].tr, ntrack);
						df[1] =  
						    l2(track[keys[keyn].nts[1].tr].keys[keyn][keys[keyn].nts[1].nn].time,
								truetime, 
								keys[keyn].nts[1].tr, ntrack);
						df[2] = INT_MAX;
						if (keys[keyn].nts[2].tr <= maxtrack)
						{							
							df[2] = l2(track[keys[keyn].nts[2].tr].keys[keyn+8][keys[keyn].nts[2].nn].time,
								truetime, 
								keys[keyn].nts[2].tr, ntrack);
						}
						char mbp {1};
						if(std::abs(df[1]) > std::abs(df[0]))
							mbp = 0;
						if (std::abs(df[2]) < std::abs(df[mbp]))
							mbp = 2;

						/*os << truetime << ' ' << ntrack << ' ' << track[ntrack].meas << '\t' << track[keys[keyn].nts[0].tr].keys[keyn][keys[keyn].nts[0].nn].time <<
						' ' << keys[keyn].nts[0].tr << '\t' << track[keys[keyn].nts[1].tr].keys[keyn][keys[keyn].nts[1].nn].time << ' ' << 
						keys[keyn].nts[1].tr << '\t' << "[ "<< (int)mbp << ' ' << df[0] << ' ' << df[1] << " ]\n";*/
						//std::cout << (int)mbp << '\n';
						short x {
							track[keys[keyn].nts[mbp].tr].keys[keyn + (mbp==2?8:0)][keys[keyn].nts[mbp].nn].note };
						if(!play_sound(xaud.keys, x)) 
							std::cout << "Failed to play sound " << x << ".\n";
						if (mbp < 2 && keys[keyn].nts[1].prn != 2 && keys[keyn].nts[mbp].prn == 0
							&& std::abs(df[mbp]) < 14.0*rel) 
						{
							jut = 64;
							if(auto x = std::abs(df[mbp])/rel; x < 1 )
								{combo++; jun = 0;}
							else if ( x < 3 )
								{combo++; jun = 1;}
							else if ( x < 6 )
								{combo++;jun = 2;}
							else if ( x < 9 )
								jun = 3;
							else tjun = 4;
							if (combo > maxcombo) maxcombo = combo;
							if (!tjun)
								hitcount[jun]++;
							else hitcount[tjun]++;
							keys[keyn].nts[mbp].prn = 1;

						}
					}
				}
			}
		}

		if (stoptime)
			goto skiptoend;
		for(int i{0}; i < nkeys; i++)
		{
			int atr {};
			unsigned char hack_i = (i==5?0:(i<6)?i+1:i);
			for(int trnow {ntrack}; ; ++trnow) 
			{		
				if (i == 0)
				{
					SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
					int trackline { trr.h + trr.y - ((191.0*track[trnow].meas + atr - ctick) * fspeed) };
					if (trackline > trr.y && trackline < trr.y + trr.h)
						SDL_RenderDrawLine(rend, trr.x+1, trackline, trr.x+trr.w-1, trackline);   				  
				}																				
				for(auto x: track[trnow].keys[i])
				{
					if (1/*x.time*track[trnow].meas + atr < ctick + ((trr.h+trr.y/fspeed)/track[trnow].meas)
					&& x.time*track[trnow].meas + atr >= ctick*/)
					{
						if(int Y { trr.h + trr.y - ((x.time*track[trnow].meas + atr - ctick) * fspeed) };
							Y > trr.y && Y < trr.y + trr.h)
						{
							if (hack_i == 0)
								SDL_SetRenderDrawColor(rend, 255, 64, 64, 255);
							else if (hack_i%2 == 1)
								SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
							else
								SDL_SetRenderDrawColor(rend, 64, 64, 255, 255);
							SDL_Rect r { keysrs[hack_i].x, Y, keysrs[hack_i].w, trr.h/96 };
							SDL_RenderFillRect(rend, &r);
						}
					}
					if (trnow == ntrack && truetime + (8*rel) >= x.time && truetime - (8*rel) <= x.time) 
					{
						SDL_SetRenderDrawColor(rend, 255, 164, 0, 168);
						SDL_RenderFillRect(rend, &keysrs[hack_i]);
					}
				}		
				/*if((truetime + ((trr.h+trr.y/fspeed)*track[trnow].meas) - (atr += 192.0*track[trnow].meas)) < 0) 			
					break;*/
				if (((192.0*track[trnow].meas + atr - ctick) * fspeed) > trr.y + trr.h)
					break;
				else atr += 192.0*track[trnow].meas;																
			}
			if(keys[i].prk > 0)
			{
				if(keys[i].prk < 127)
					keys[i].prk--;
				if (i == 5) {
					SDL_RenderCopy(rend, lttx[0], 0, &ltr[0]);
				}
				else {
					int n {i};
					if (i < 6)
						n++;
					SDL_RenderCopy(rend, lttx[n%2+1], 0, &ltr[n]);
				}
			}
			if (1&&keys[i].nts[0].nn != keys[i].nts[1].nn)
			{
				int df[2];
				df[0] =   
					l2(track[keys[i].nts[0].tr].keys[i][keys[i].nts[0].nn].time,
						truetime,
						keys[i].nts[0].tr, ntrack);
				df[1] = 
					l2(track[keys[i].nts[1].tr].keys[i][keys[i].nts[1].nn].time,
						truetime,
						keys[i].nts[1].tr, ntrack);
				if ((std::abs(df[0]) > 9.0*curbpm/120.0 || std::abs(df[0]) > std::abs(df[1])) 
					&& keys[i].nts[0].prn == 0)
				{
					
					keys[i].nts[0].prn = 1;
					hitcount[4]++;
					tjun = 4;
					jut = 64*rel;
				}
			}

		}


		if (tjun==4)
		{
			if(miss.bga && (miss.i >= miss.c || miss.i == (unsigned char)-1))
			{
				miss.n = 0;
				miss.c = (*miss.bga)[miss.bga->size()-1].time;
				if (miss.c < 96)
				miss.c = 96*rel;
			}
			miss.i = 0;
			jun = tjun;
			combo = 0;
		}
		if(--jut >= 0 && jun >= 0)
		{			
			SDL_Rect r[6];
			r[5].x = r[5].w = 0; 
			int t[6] {0,0,0,0,0,0};
			char nmst { prepare_num(fnt, r, t, combo) };
			judger[jun].x = trr.w*0.2;
			judger[jun].y = trr.h*0.7;
			judger[jun].x = trr.x + (trr.w - (judger[jun].w + r[5].x + r[5].w))/2;
			SDL_RenderCopy(rend, jutex[jun], 0, &judger[jun]);
			//if (!(nmst == 5 && t[nmst] == 0))
			for (int i {nmst}; i < 6; i++)
			{ 	
				r[i].x += judger[jun].w + judger[jun].x;
				r[i].y = judger[jun].y - r[i].y + judger[jun].h/2;
				SDL_RenderCopy(rend, numtex[t[i]], 0, &r[i]);
			}
		}
		else jun = -1;
		SDL_FillRect(bgasur, 0, SDL_MapRGB(bgasur->format, 0,0,0));
		if(miss.bga && miss.i <= miss.c)
		{
			miss.i++;
			if (miss.n+1 < miss.bga->size() && miss.i*track[ntrack].meas == (*miss.bga)[miss.n+1].time)
				miss.n++;
			SDL_BlitSurface(imgl[(*miss.bga)[miss.n].note].s, 0, bgasur, 0);
		}
		else
		{
			if (curbga4) SDL_BlitSurface(curbga4, 0, bgasur, 0);
			if (curbga7) SDL_BlitSurface(curbga7, 0, bgasur, 0);
		}
		SDL_LockTexture(bgatex, 0, &bgapix, &bgapit);
		std::memcpy(bgapix, bgasur->pixels, bgasur->pitch*256);
		bgapit = bgasur->pitch;
		SDL_UnlockTexture(bgatex);
		SDL_RenderCopy(rend, bgatex, 0, &bgar);

		SDL_RenderPresent(rend);

		/*****END*****/
		ctick++;
		if (track[ntrack].bpmv.size() && stoptime == 0)
		for (auto x: track[ntrack].bpmv)
			if(bool isstop {x.time - (1<<10) >= truetime && x.time - (1<<10) < neartime}; 
				(x.time >= truetime && x.time < neartime) || isstop) 
			{
				if (isstop)
					stoptime = x.note;
				else ft.QuadPart = - 120 / (curbpm = x.note)  * 10e4;
				bpmnst = prepare_num(fnt, bpmnums, bpmns, curbpm);
				for (int i {bpmnst}; i < 6; i++)
				{
					bpmnums[i].x += bpmr.x + bpmr.h;
					bpmnums[i].y = bpmr.y - bpmnums[i].y + bpmr.h/2;
				}
			}	
		skiptoend:
		if (stoptime)
		{ 
			--stoptime;
			if (stoptime == 1) { stoptime = 0; ctick++; } 
		}
	
		if (truetime > 191)
		{
			ctick=0;
			++ntrack;
			if(ntrack <= maxtrack)
				curproc += ppt;
		}

		WaitForSingleObject(timer_driver, INFINITE);
	}
	//os.close();
	if (autoplay)
		goto skipeval;
	std::cout << "Maxcombo: " << maxcombo << " / " << totalnotes << std::endl;
	std::cout.precision(2);
	std::cout << "Rate: " 
		<< (100.0 * (double)(hitcount[0]*4 + hitcount[1]*3 + hitcount[2] * 2 + hitcount[3] * 1)/(totalnotes*4.0))
		<< ' ' << '%' << std::endl;
	std::cout.precision(6);
	std::cout << "CL: " << hitcount[0] << std::endl
			  << "GR: " << hitcount[1] << std::endl
			  << "GD: " << hitcount[2] << std::endl
			  << "BD: " << hitcount[3] << std::endl
			  << "PR: " << hitcount[4] << std::endl;

	skipeval:
	return 0;
}
	

